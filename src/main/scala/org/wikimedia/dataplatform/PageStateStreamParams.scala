package org.wikimedia.dataplatform

import scala.concurrent.duration._
import scala.jdk.CollectionConverters._
import scala.language.postfixOps

case class PageStateStreamParams(
                                  windowLength: FiniteDuration = 30 seconds,
                                  outOfOrderness: FiniteDuration = 30 seconds,
                                  brokers: List[String] = "kafka-jumbo1002.eqiad.wmnet:9092" :: Nil,
                                  outputBrokers: List[String] = "kafka-test1006.eqiad.wmnet:9092" :: Nil,
                                  consumerGroup: String = "platform-event-driven-page-state-poc",
                                  mwInternalEndpoint: String = "https://api-ro.discovery.wmnet",
                                  eventStreamConfigUri: String = getClass.getResource("/stream_config/config.json").toString,
                                  eventSchemaBaseUris: java.util.List[String] = Seq(
                                    "https://schema.wikimedia.org/repositories/primary/jsonschema",
                                    getClass.getResource("/schemas").toString,
                                    "https://schema.wikimedia.org/repositories/secondary/jsonschema").asJava,
                                  eventStreams: Iterable[String] = Seq(
                                    "mediawiki.page-delete",
                                    "mediawiki.page-suppress",
                                    "mediawiki.page-undelete",
                                    "mediawiki.revision-create"),
                                  outputStream: String = "dcausse.mediawiki.page-state-stream",
                                  httpRoutes: java.util.Map[String, String] = Map("https://schema.wikimedia.org" -> "https://schema.discovery.wmnet").asJava
                                )
