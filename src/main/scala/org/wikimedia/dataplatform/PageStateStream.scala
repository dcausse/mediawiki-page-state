package org.wikimedia.dataplatform

import java.time.Instant
import java.util.UUID

import scala.collection.mutable.ListBuffer
import scala.jdk.OptionConverters._
import scala.language.implicitConversions

import io.findify.flink.api._
import io.findify.flink.api.function.ProcessWindowFunction
import io.findify.flinkadt.api._
import org.apache.flink.api.common.typeinfo.TypeInformation
import org.apache.flink.configuration.Configuration
import org.apache.flink.streaming.api.functions.sink.{DiscardingSink, SinkFunction}
import org.apache.flink.streaming.api.windowing.assigners.TumblingEventTimeWindows
import org.apache.flink.streaming.api.windowing.time.Time
import org.apache.flink.streaming.api.windowing.windows.TimeWindow
import org.apache.flink.table.api.{EnvironmentSettings, Table, TableDescriptor, TableEnvironment, TablePipeline}
import org.apache.flink.table.api.bridge.java.StreamTableEnvironment
import org.apache.flink.types.Row
import org.apache.flink.util.Collector
import org.wikimedia.eventutilities.core.event.EventStreamFactory
import org.wikimedia.eventutilities.flink.table.EventTableDescriptorBuilder

object PageStateStream {
  case class PageId(wiki: String, pageId: Long)

  def main(args: Array[String]): Unit = {
    val config = new Configuration()
    config.setString("table.exec.source.idle-timeout", "30 s")
    val params: PageStateStreamParams = PageStateStreamParams()
    val streamEnv = StreamExecutionEnvironment.getExecutionEnvironment
    val settings = EnvironmentSettings.newInstance()
        .inStreamingMode().withConfiguration(config).build();
    implicit val tableEnv: StreamTableEnvironment = StreamTableEnvironment.create(streamEnv.getJavaEnv, settings)
    val outputTable = buildTableDescriptor(params, "mediawiki.page-state", params.outputBrokers, false)

    val outputTypeInfo = tableDescToTypeInfo(outputTable, tableEnv)
    val inputEventStreams: DataStream[Row] = join(
        params.eventStreams
          .map(eventStreamAsDataStream(_, params))
          .map(transformToPageStream(_)(outputTypeInfo))
    )
    val lateTag = new OutputTag[Row]("late-events")(inputEventStreams.dataType)
    val reorderedStream = reorder(inputEventStreams.keyBy(keySelector), lateTag, params.windowLength.toSeconds)

    reorderedStream.getSideOutput(lateTag)(outputTypeInfo)
      .addSink(new DiscardingSink[Row]).setDescription("late-event (discarded)")

    val tablePipeline = tableEnv.fromDataStream(reorderedStream.javaStream).insertInto(outputTable)

    // XXX: what to do here?
    // running tablePipeline.execute() will launch the insert will ignore the side-output sink

    // running streamEnv.execute("page-state-stream") will run the tumbling window and the side-output but won't insert
    // into the output table....
    tablePipeline.execute()
  }

  def keySelector: Row => PageId = r => PageId(r.getFieldAs[String]("database"), r.getFieldAs[Long]("page_id"))
  def keyTypeInfo: TypeInformation[PageId] = deriveTypeInformation
  def eventStreamAsDataStream(streamName: String, params: PageStateStreamParams)(implicit tableEnv: StreamTableEnvironment): DataStream[Row] = {
    new DataStream(tableEnv.toDataStream(buildTable(params, tableEnv, streamName)))
  }

  def join(dataStreams: Iterable[DataStream[Row]]): DataStream[Row] = dataStreams match {
    case Nil => throw new NoSuchElementException("at least one stream is needed")
    case x :: Nil => x
    case x :: rest => x.union(rest: _*)
  }

  def reorder(stream: DataStream[Row], lateEventOutputTag: OutputTag[Row], windowLengthSec: Long): DataStream[Row] = {
    stream.keyBy(keySelector).window(TumblingEventTimeWindows.of(Time.seconds(windowLengthSec)))
      .sideOutputLateData(lateEventOutputTag)
      .process(new ProcessWindowFunction[Row, Row, PageId, TimeWindow] {
        override def process(key: PageId, context: Context, inputEvents: Iterable[Row], out: Collector[Row]): Unit = {
          val toReorder = new ListBuffer[Row]()
          for (event <- inputEvents) {
            toReorder.append(event)
          }
          toReorder
            // XXX: fix and use a real watermark field
            .sortBy(e => (e.getFieldAs[Long]("rev_id"), e.getFieldAs[Row]("meta").getFieldAs[String]("dt")))
            .foreach(out.collect)
        }
      })(stream.dataType)
  }

  val pageDelete: Map[String, Row => Any] = Map(
    "comment" -> cp("comment"),
    "database" -> cp("database"),
    "page_id" -> cp("page_id"),
    "page_is_redirect" -> cp("page_is_redirect"),
    "page_namespace" -> cp("page_namespace"),
    "page_title" -> cp("page_title"),
    "parsedcomment" -> cp("parsedcomment"),
    "performer" -> cp("performer"),
    "rev_id" -> cp("rev_id"),
    "page_action" -> (_ => "page-delete")
  )
  val fieldsMap: Map[String, Map[String, Row => Any]] = Map(
    "mediawiki.revision-create" -> Map(
      "comment" -> cp("comment"),
      "database" -> cp("database"),
      "page_id" -> cp("page_id"),
      "page_is_redirect" -> cp("page_is_redirect"),
      "page_namespace" -> cp("page_namespace"),
      "page_title" -> cp("page_title"),
      "parsedcomment" -> cp("parsedcomment"),
      "performer" -> cp("performer"),
      "rev_content_format" -> cp("rev_content_format"),
      "rev_content_model" -> cp("rev_content_model"),
      "rev_id" -> cp("rev_id"),
      "rev_is_revert" -> cp("rev_is_revert"),
      "rev_len" -> cp("rev_len"),
      "rev_minor_edit" -> cp("rev_minor_edit"),
      "rev_parent_id" -> cp("rev_parent_id"),
      "rev_revert_details" -> cp("rev_revert_details"),
      "rev_sha1" -> cp("rev_sha1"),
      "rev_timestamp" -> cp("rev_timestamp"),
      "page_action" -> (r => Option(r.getField("rev_parent_id")) match {
        case Some(_) => "page-update"
        case None => "page-create"
      })
    ),
    "mediawiki.page-delete" -> pageDelete,
    "mediawiki.page-suppress" -> pageDelete,
    "mediawiki.page-undelete" -> Map(
      "comment" -> cp("comment"),
      "database" -> cp("database"),
      "page_id" -> cp("page_id"),
      "page_is_redirect" -> cp("page_is_redirect"),
      "page_namespace" -> cp("page_namespace"),
      "page_title" -> cp("page_title"),
      "parsedcomment" -> cp("parsedcomment"),
      "performer" -> cp("performer"),
      "rev_id" -> cp("rev_id"),
      "page_action" -> (_ => "page_restore")
    )
  )

  def transformToPageStream(stream: DataStream[Row])(outputShape: TypeInformation[Row]): DataStream[Row] = {
    stream.map(source => {
      val dest = Row.withNames()
      val meta = source.getFieldAs[Row]("meta")
      val newMeta = Row.withNames()
      newMeta.setField("stream", "mediawiki.page-state")
      newMeta.setField("id", UUID.randomUUID().toString)
      newMeta.setField("request_id", meta.getFieldAs[String]("request_id"))
      // FIXME: find a way to propage the watermark
      newMeta.setField("dt", meta.getFieldAs[String]("dt"))
      // FIXME: some part of this should be handled by the event generator
      dest.setField("meta", newMeta)
      dest.setField("$schema", "/mediawiki/page/state/1.0.0")
      val stream: String = meta.getFieldAs("stream")
      fieldsMap.get(stream) match {
        case Some(map) => map foreach {
          case (k, fieldValue) => Option(fieldValue(source)) match {
            case Some(value) => dest.setField(k, value)
            case None =>
          }
        }
        case None => throw new UnsupportedOperationException(s"Unsupported stream [$stream]")
      }
      dest
    })(outputShape)
  }

  def cp(fromField: String): Row => Any = {
    r: Row => r.getField(fromField)
  }

  def tableDescToTypeInfo(tableDesc: TableDescriptor, tableEnv: StreamTableEnvironment): TypeInformation[Row] = {
    // XXX: this is a painful hack to obtain the resulting TypeInformation[Row] of the output table
    tableEnv.createTemporaryTable("dummy", tableDesc)
    tableEnv.toDataStream(tableEnv.from("dummy")).getType
  }

  def buildTableDescriptor(params: PageStateStreamParams, eventStream: String, brokers: List[String], withTimestamp: Boolean = true): TableDescriptor = {
    val eventStreamFactory = EventStreamFactory.builder()
      .setEventStreamConfig(params.eventStreamConfigUri)
      .setEventSchemaLoader(params.eventSchemaBaseUris)
      .setHttpRoutes(params.httpRoutes)
      .build()
    val builder: EventTableDescriptorBuilder = new EventTableDescriptorBuilder(eventStreamFactory)
      .eventStream(eventStream)

    if (withTimestamp) {
      builder.withKafkaTimestampAsWatermark("kafka_timestamp", params.outOfOrderness.toSeconds.intValue)
    }

    builder.setupKafka(brokers.mkString(","), params.consumerGroup)
      .build()
  }

  def buildTable(params: PageStateStreamParams, tableEnvironment: TableEnvironment, eventStream: String): Table = {
    tableEnvironment.from(buildTableDescriptor(params, eventStream, params.brokers))
  }
}
