package dataplatform

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers
import org.wikimedia.dataplatform.{PageStateStream, PageStateStreamParams}
import org.wikimedia.eventutilities.core.event.{EventStreamConfig, EventStreamFactory}

class TestLocalSchema extends AnyFlatSpec with Matchers {
  "local schema" should "be loaded" in {
    val streamConfig = EventStreamConfig.builder()
      .setEventStreamConfigLoader(PageStateStream.getClass.getResource("/stream_config/config.json").toString)
      .build()

    val params = PageStateStreamParams()
    val eventStreamFactory = EventStreamFactory.builder()
      .setEventStreamConfig(params.eventStreamConfigUri)
      .setEventSchemaLoader(params.eventSchemaBaseUris)
      .build()

    val eventStream = eventStreamFactory.createEventStream("mediawiki.page-delete")
  }
}
